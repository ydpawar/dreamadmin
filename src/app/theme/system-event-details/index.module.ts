import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {SystemEventDetailsComponent} from './index.component';
import {SystemJackpotDetailsComponent} from './jackpot.component';


const routes: Routes = [
  {
    path: ':sport/:eid',
    component: SystemEventDetailsComponent,
  },
  {
    path: ':sport/:eid/:uid',
    component: SystemEventDetailsComponent,
  },
  {
    path: 'jackpot-details/:eid/:type/:mid',
    component: SystemJackpotDetailsComponent,
  },
  {
    path: 'jackpot-details/:eid/:type/:mid/:uid',
    component: SystemJackpotDetailsComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    SystemEventDetailsComponent, SystemJackpotDetailsComponent
    ]
})
export class SystemEventDetailsModule {
}
