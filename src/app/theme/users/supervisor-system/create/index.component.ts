import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SupervisorService } from '../../../../_api/index';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastrService } from 'src/app/_services';

// interface CreateSuperviosrSystemManage {
//   id: string;
//   name: string;
//   username: string;
//   status: {
//     text: string;
//     cssClass: string;
//   };
// }

// class CreateSuperviosrSystemManageObj implements CreateSuperviosrSystemManage {
//   id: string;
//   name: string;
//   username: string;
//   status: {
//     text: string;
//     cssClass: string;
//   };
// }

interface SessionUserManage {
  id: string;
  name: string;
  username: string;
  userrole: any;
  status: {
    text: string;
    cssClass: string;
  };
}

class SessionUserManageObj implements SessionUserManage {
  id: string;
  name: string;
  username: string;
  userrole: any;
  status: {
    text: string;
    cssClass: string;
  };
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [SupervisorService]
})
export class CreateComponent implements OnInit, AfterViewInit {

  title = 'System Supervisor- Create';
  breadcrumb: any = [{ title: 'Supervisor-System', url: '/' }, { title: 'Create', url: '/' }];

  frm: FormGroup;
  userError: string;
  parentUser: string;

  aList: SessionUserManage[] = [];
  marketList: SessionUserManage[] = [];
  sportList: SessionUserManage[] = [];
  actionList: SessionUserManage[] = [];

  userroleList: SessionUserManage[] = [];
  userList: SessionUserManage[] = [];
  isEmpty = false;
  tempArr: any = [];
  tempMarketArr: any = [];
  tempActionArr: any = [];
  tempsportVal: string;
  tempmarketVal: string;
  tempactionVal: string;
  username: string;
  userrole: any;
  name: string;
  uid: number;


  constructor(
    private formBuilder: FormBuilder,
    private service: SupervisorService,
    private router: Router,
    private route: ActivatedRoute,
    // tslint:disable-next-line:variable-name
    private _location: Location,
    private toastr: ToastrService
  ) {
    this.createForm();
    this.uid = this.route.snapshot.params.id;
    if (this.uid) { this.title = 'Supervisor System - Update' }
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  applyFilters() {
    const data = '';
    if (this.uid) {
      this.service.sessionSportList({ id: this.uid }).subscribe((res) => this.onSuccessEdit(res));
    } else {
      this.service.sessionSportList(data).subscribe((res) => this.onSuccessResult(res));
    }
  }

  onSuccessResult(res) {
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      // this.isLoading = false;
      const items = res.data;
      this.actionList = res.data.actionList;
      this.sportList = res.data.sportList;
      this.marketList = res.data.marketList;
      // this.aList = items;

    } else {
      this.isEmpty = true;
    }
  }

  onSuccessEdit(res) {

    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data !== undefined) {
        const items = res.data;
        this.actionList = res.data.actionList;
        this.sportList = res.data.sportList;
        this.marketList = res.data.marketList;
        this.userList = res.data.userList;
        this.userroleList = res.data.userroleList;
        // this.aList = items;
        this.username = res.data.userList.username;
        this.name = res.data.userList.name;
        this.userrole = res.data.userList.role;

        let item;
        let items2;
        item = res.data.userroleList.market_role;
        if (item !== 0) {
          for (items2 of item) {
            this.tempMarketArr.push(items2);
          }
        }

        let sportrole;
        let sportroles;
        sportrole = res.data.userroleList.sport_role;
        if (sportrole !== 0) {
          for (sportroles of sportrole) {
            this.tempArr.push(sportroles);
          }
        }

        let actionrole; let actionroles;
        actionrole = res.data.userroleList.action_role;
        if (actionrole !== 0) {
          for (actionroles of actionrole) {
            this.tempActionArr.push(actionroles);
          }
        }

        this.frm.patchValue({
          name: this.name,
          username: this.username,
          sportVal: [''],
          marketVal: [''],
          actionVal: [''],
        });
      } else {
        this.isEmpty = true;
      }
    }
  }


  createForm() {
    this.frm = this.formBuilder.group({
      name: ['', [Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20)]],
      username: ['', [Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20)]],
      password: ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
        Validators.pattern(new RegExp('^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\$%\^&\*]).{6,20})'))]],
      assignsport: ['', Validators.required],
      assignmarket: ['', Validators.required],
      assignresult: [''],
      sportVal: [''],
      marketVal: [''],
      actionVal: [''],
      // remark: [''],
    });
  }

  onChangeSport(event, cat: any) { // Use appropriate model type instead of any
    if (event.target.checked) {
      this.tempArr.push(cat.id);
    } else {
      let item;
      let items2;
      item = this.tempArr;
      this.tempArr = [];
      for (items2 of item) {
        if (items2 !== cat.id) {
          this.tempArr.push(items2);
        }
      }
    }
    if (this.tempsportVal === undefined || this.tempsportVal === '') {
      this.tempsportVal = cat.id;
    } else {
      this.tempsportVal += ',' + cat.id;
    }
  }


  onChangeMarket(event, cat: any) { // Use appropriate model type instead of any
    if (event.target.checked) {
      this.tempMarketArr.push(cat.id);
    } else {
      let item;
      let items2;
      item = this.tempMarketArr;
      this.tempMarketArr = [];
      for (items2 of item) {
        if (items2 !== cat.id) {
          this.tempMarketArr.push(items2);
        }
      }
    }

    if (this.tempmarketVal === undefined || this.tempmarketVal === '') {
      this.tempmarketVal = cat.id;
    } else {
      this.tempmarketVal += ',' + cat.id;
    }
  }

  onChangeAction(event, cat: any) { // Use appropriate model type instead of any
    if (event.target.checked) {
      this.tempActionArr.push(cat.id);
    } else {
      let item;
      let items2;
      item = this.tempActionArr;
      this.tempActionArr = [];
      for (items2 of item) {
        if (items2 !== cat.id) {
          this.tempActionArr.push(items2);
        }
      }
    }

    if (this.tempactionVal === undefined || this.tempactionVal === '') {
      this.tempactionVal = cat.id;
    } else {
      this.tempactionVal += ',' + cat.id;
    }
  }

  submitForm() {
    const data = this.getFormData();
    if (this.uid) {
      data.id = this.uid
      this.service.updateSystem(data).subscribe((res) => this.onSuccess(res));
    } else {
      if (this.frm.valid) {
        this.service.createSystem(data).subscribe((res) => this.onSuccess(res));
      } else {
        this.toastr.error('please try again !!', 'something went wrong');
      }
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this._location.back();
      // this.router.navigate(['/manage']);
    }
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    this.frm.patchValue({
      uid: this.uid, sportVal: this.tempArr, marketVal: this.tempMarketArr, actionVal: this.tempActionArr // , marketVal: this.tempMarketArr
    });
    const data = this.frm.value;
    return data;
  }

  get frmName() { return this.frm.get('name'); }
  get frmUsername() { return this.frm.get('username'); }
  get frmPassword() { return this.frm.get('password'); }
  // get frmRemark() { return this.frm.get('remark'); }

}

