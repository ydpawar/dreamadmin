import { Component, OnInit, AfterViewInit, OnDestroy, Injector } from '@angular/core';
import { ActionService, SupervisorService } from '../../../../_api/index';
import { AuthIdentityService } from '../../../../_services/index';
import { BaseComponent } from '../../../../common/commonComponent';
import { Validators } from '@angular/forms';

declare var $, swal;

interface SessionUserManage {
  id: string;
  name: string;
  username: string;
  user_rights: any;

  status: {
    text: string;
    cssClass: string;
    class2: string;
  };
}

class SessionUserManageObj implements SessionUserManage {
  id: string;
  name: string;
  username: string;
  // tslint:disable-next-line:variable-name
  user_rights: any;
  status: {
    text: string;
    cssClass: string;
    class2: string;
  };
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [SupervisorService, ActionService]
})

export class ManageComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  page = { start: 1, end: 5 };
  user: any;
  aList: SessionUserManage[] = [];
  cItem: SessionUserManage;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_sv_s';

  title = 'System Supervisor';
  createUrl = '/users/supervisor-system/create';
  breadcrumb: any = [{ title: 'Supervisor - System', url: '/' }, { title: 'Manage', url: '/' }];
  pageLoad: any;

  idRp:any;
  constructor(
    inj: Injector,
    private service: SupervisorService,
    private service2: ActionService,
  ) {
    super(inj);
    const auth = new AuthIdentityService();
    this.createForm();

    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
      // tslint:disable-next-line:triple-equals
      if (this.user.role != 'ADMIN' && this.user.role != 'ADMIN2' && this.user.role != 'SM1') {
        this.back();
      }
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.manageSystem().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1) {
      // this.isLoading = false;
      if (res.data !== null && res.data !== undefined && res.data !== undefined) {
        $('#DataTables_sv_s').dataTable().fnDestroy();
        const items = res.data;
        const data: SessionUserManage[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: SessionUserManage = new SessionUserManageObj();

            cData.id = item.id;
            cData.name = item.name;
            cData.username = item.username;
            cData.user_rights = item.user_rights;
            cData.status = {
              text: item.status === 1 ? 'Active' : 'Inactive',
              cssClass: item.status === 1 ? 'success' : 'dark',
              class2: item.status === 1 ? 'mdi-check' : 'mdi-close'
            }

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.initDataTables('DataTables_sv_s');
        setTimeout(() => {
          this.spinner.hide();
        }, 500);
      } else {
        this.spinner.hide();
      }
    }
  }

  changeStatus(item: SessionUserManage) {
    this.cItem = item;
    const status = item.status.text === 'Active' ? 'Inactive' : 'Active';

    swal.fire({
      title: 'You want to ' + status + ' ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus('status');
        }
      });
  }

  deleteStatus(item: SessionUserManage) {
    this.cItem = item;

    swal.fire({
      title: 'You want to delete ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus('delete');
        }
      });
  }

  initStatus(type) {
    if (type === 'delete') {
      this.service.changeuserstatus(this.cItem.id, type).subscribe((res) => this.onChangeStatusSuccess(res));
    } else {
      this.service.changeuserstatus(this.cItem.id, this.cItem.status.text).subscribe((res) => this.onChangeStatusSuccess(res));
    }
  }


  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  resetPassword(id) {
    this.idRp = id;
    console.log(this.idRp)
    this.frm.reset();
    $('.resetPassword').modal('show');
    $('body').removeClass('modal-open');
  }

  createForm() {
    this.frm = this.formBuilder.group({
      uid: [''],
      password: ['', Validators.required],
    });
  }

  submitForm() {
    const data = this.frm.value;
    data.uid = this.idRp
    if (this.frm.valid) {
      this.service.resetPassword(data).subscribe((res) => this.onSuccessRP(res));
    }
  }

  onSuccessRP(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.onCancel();
    }
  }


  onCancel() {
    this.frm.reset();
    $('.resetPassword').modal('hide');
    $('body').removeClass('modal-open');
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmPassword() {
    return this.frm.get('password');
  }

}

