import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { WithdrawalComponent } from './index.component';

const routes: Routes = [
  {
    path: 'withdrawal/:uid',
    component: WithdrawalComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    WithdrawalComponent
  ]
})
export class WithdrawalModule {

}
