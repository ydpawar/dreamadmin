import { Component, OnInit } from '@angular/core';
import {ActionService, DashboardService} from '../../_api';
import {AuthIdentityService, ToastrService} from '../../_services';
import swal from "sweetalert2";
import {NgxSpinnerService} from 'ngx-spinner';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

declare var $;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DashboardService, ActionService]
})
export class HomeComponent implements OnInit {
  frm: FormGroup;
  dataList: any = [];
  eventList: any = [];
  marketList: any = [];
  marketIdsArr: any = [];
  isActiveTabs: string;
  isEmpty = true;
  cUserData: any;
  searchUserData: any;
  searchUserError = false;
  bannerUrl: string;

  title = 'Dashboard';
  breadcrumb: any = [];
  // breadcrumb: any = [{title: 'Manage', url: '/' }];

  // tslint:disable-next-line:max-line-length
  constructor(
      private formBuilder: FormBuilder,
      private service: DashboardService,
      private service2: ActionService,
      private authIdentity: AuthIdentityService,
      private spinner: NgxSpinnerService,
      private toaster: ToastrService) {
      this.isActiveTabs = 'all';
  }

  ngOnInit() {
    this.createForm();
    this.getDataList(this.isActiveTabs);
    if ( window.localStorage.getItem('loadBanner') && window.localStorage.getItem('loadBanner') === 'yes' ) {
      $('.modal-s3').modal('show');
      $('body').removeClass('modal-open');
      window.localStorage.removeItem('loadBanner');
    }
  }

  async getDataList(type) {
    this.spinner.show();
      await this.service.getList(type).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        (data) => {
          this.onSuccessDataList(data);
        },
        error => {
          // this.toaster.error('Error in Get DataList Api !!', 'Something want wrong..!');
        });
  }

  onSuccessDataList(response) {
    if (response.status !== undefined) {
      if (response.status === 1) {
        this.isEmpty = true;
        this.eventList = this.marketList = this.dataList = [];

        if ( response.data.length > 0 ) {
          this.dataList = response.data;
        }

        if ( response.userData ) {
          this.cUserData = response.userData;
        }
        if ( response.bannerUrl ) {
          this.bannerUrl = response.bannerUrl;
        }
        if ( response.event && response.event.length > 0 ) {
            this.isEmpty = false;
            this.eventList = response.event;
        }

        if ( response.sport.length > 0 ) {
          this.isEmpty = false;
          this.marketList = response.sport;
        }

      }
    }
    this.spinner.hide();
  }

  doEventBlock(eid, status) {
    let statustxt = 'block';
    if ( status ) {
      statustxt = 'unblock';
    }
    swal.fire({
      title: 'Are you sure to want ' + statustxt + ' ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service.doEventBlock(eid).subscribe((res) => this.onSuccessEventBlock(res));
      }
    });
  }

  onSuccessEventBlock(res) {
    if (res.status === 1) {
      this.getDataList(this.isActiveTabs);
    }
  }

  doSportBlock(eid, status) {
    let statustxt = 'block';
    if ( status ) {
      statustxt = 'unblock';
    }
    swal.fire({
      title: 'Are you sure to want ' + statustxt + ' ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service.doSportBlock(eid).subscribe((res) => this.onSuccessSportBlock(res));
      }
    });
  }

  onSuccessSportBlock(res) {
    if (res.status === 1) {
      this.getDataList(this.isActiveTabs);
    }
  }

  doStatusUpdate(uid, typ) {
    swal.fire({
      title: 'Are you sure to change this status ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        if (typ === 1) {
          this.service2.doBlockUnblock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        } else {
          this.service2.doLockUnlock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        }
      }
    });
  }

  doStatusDelete(uid) {
    swal.fire({
      title: 'Are you sure to delete ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service2.doDelete(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
      }
    });
  }

  onSuccessStatusUpdate(res) {
    if (res.status === 1) {
      this.submitForm();
    }
  }

  createForm() {
    this.frm = this.formBuilder.group({
      username: ['', [ Validators.required,
        Validators.minLength(2) ] ]
    });
  }

  submitForm() {
    const data = this.frm.value;
    if (this.frm.valid) {
      this.service.searchUserData(data).subscribe((res) => this.onSearch(res));
    }
  }

  onSearch(res) {
    if (res.status === 1 && res.data != null) {
      this.searchUserError = false;
      this.searchUserData = res.data;
    } else {
      this.searchUserError = true;
      this.searchUserData = undefined;
    }
  }

  get frmUsername() { return this.frm.get('username'); }

}
