import {Component, OnInit, AfterViewInit} from '@angular/core';

declare var $, Swal;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit, AfterViewInit {

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.loadScript();
  }

  loadScript() {
    $('table.dt-multi-display').DataTable();
    $('#ScrollVerticalPlus, #ScrollVerticalMinus').DataTable({
      'scrollY': '400px',
      'scrollCollapse': true,
      'paging': false
    });

    $('#DashButtonGame').click(() => {
      Swal.fire({
        title: 'Are you sure?',
        text: 'You want to block Cricket!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success mt-2',
        cancelButtonClass: 'btn btn-danger ml-2 mt-2',
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          Swal.fire({
            title: 'Deleted!',
            text: 'Your Game Block.',
            type: 'success'
          });
        } else if ( // Read more about handling dismissals
          result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire({
            title: 'Cancelled',
            text: 'Your imaginary file is safe :)',
            type: 'error'
          });
        }
      });
    });
  }

}
